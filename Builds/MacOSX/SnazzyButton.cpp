//
//  SnazzyButton.cpp
//  JuceBasicWindow
//
//  Created by Duncan Whale on 10/30/14.
//
//

#include "SnazzyButton.h"

int buttonState = 0;

void SnazzyButton::paint(Graphics& g)
{
    for (int i = 0; i < 4; i++)
    {
        if (i == 0 && buttonState == 0)
            g.setColour(Colours::hotpink);
        else if (i == 1 && buttonState == 0)
            g.setColour(Colours::yellow);
        else if (i == 2 && buttonState == 0)
            g.setColour(Colours::green);
        else if (i == 3 && buttonState == 0)
            g.setColour(Colours::red);
        else if (i == 0 && buttonState == 1)
            g.setColour(Colours::lightpink);
        else if (i == 1 && buttonState == 1)
            g.setColour(Colours::lightyellow);
        else if (i == 2 && buttonState == 1)
            g.setColour(Colours::lightgreen);
        else if (i == 3 && buttonState == 1)
            g.setColour(Colours::pink);
        else if (i == 0 && buttonState == 2)
            g.setColour(Colours::purple);
        else if (i == 1 && buttonState == 2)
            g.setColour(Colours::brown);
        else if (i == 2 && buttonState == 2)
            g.setColour(Colours::darkgreen);
        else if (i == 3 && buttonState == 2)
            g.setColour(Colours::darkred);
        
        g.fillEllipse((getWidth() / 10.0) * i, (getHeight() / 10.0) * i, getWidth() - ((getWidth() / 10.0) * i * 2.0), getHeight() - ((getHeight() / 10.0) * i * 2.0));
    }
}

void SnazzyButton::mouseEnter (const MouseEvent& event)
{
    buttonState = 1;
    repaint();
}

void SnazzyButton::mouseExit (const MouseEvent& event)
{
    buttonState = 0;
    repaint();
}

void SnazzyButton::mouseUp (const MouseEvent& event)
{
    buttonState = 1;
    repaint();
}

void SnazzyButton::mouseDown (const MouseEvent& event)
{
    buttonState = 2;
    repaint();
}