//
//  ButtonRow.h
//  JuceBasicWindow
//
//  Created by Duncan Whale on 11/5/14.
//
//

#ifndef BUTTONROW_H
#define BUTTONROW_H

#include "../JuceLibraryCode/JuceHeader.h"
#include <iostream>
#include "SnazzyButton.h"

class ButtonRow : public Component
{
public:
    ButtonRow();
    void resized() override;
    
private:
    SnazzyButton button[5];
};

#endif /* defined(BUTTONROW_H) */
