//
//  ButtonRow.cpp
//  JuceBasicWindow
//
//  Created by Duncan Whale on 11/5/14.
//
//

#include "ButtonRow.h"

ButtonRow::ButtonRow()
{
    for (int i = 0; i < 5; i++)
        addAndMakeVisible(&button[i]);
}

void ButtonRow::resized()
{
    for (int i = 0; i < 5; i++)
        button[i].setBounds((getWidth() / 5.0) * i, 0, getWidth() / 5.0, getHeight());
}