//
//  SnazzyButton.h
//  JuceBasicWindow
//
//  Created by Duncan Whale on 10/30/14.
//
//

#ifndef SNAZZY_BUTTON_H
#define SNAZZY_BUTTON_H

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"

class SnazzyButton  : public Component
{
public:
    void paint(Graphics&) override;
    void mouseEnter (const MouseEvent&) override;
    void mouseExit (const MouseEvent&) override;
    void mouseUp (const MouseEvent&) override;
    void mouseDown (const MouseEvent&) override;
    
private:
    
};

#endif /* defined(SNAZZY_BUTTON_H) */
