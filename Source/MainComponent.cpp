/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    
//    textButton1.setButtonText ("Click me!");
//    textButton2.setButtonText ("Don't click him, click me!");
//    addAndMakeVisible(&textButton1);
//    addAndMakeVisible(&textButton2);
//    textButton1.addListener(this);
//    textButton2.addListener(this);
//    
//    slider1.setSliderStyle (Slider::LinearHorizontal);
//    addAndMakeVisible(&slider1);
//    slider1.addListener(this);
//    
//    combobox1.addItem ("Item1", 1);
//    combobox1.addItem ("Item2", 2);
//    combobox1.addItem ("Item3", 3);
//    addAndMakeVisible (&combobox1);
//    combobox1.addListener(this);
//    
//    texteditor1.setText ("Add some text here.");
//    addAndMakeVisible (&texteditor1);
//    texteditor1.addListener(this);
    
//    addAndMakeVisible (&colourSelector1);
    
//    addAndMakeVisible(&mySnazzyButton);
    
    addAndMakeVisible(&myButtonRow);
}

MainComponent::~MainComponent()
{
    
}

void MainComponent::resized()
{
    DBG("Resized: " << getWidth() << ", " << getHeight() << ".\n");
//    textButton1.setBounds (10, 10, getWidth() - 20, 40);
//    textButton2.setBounds (10, 60, getWidth() - 20, 40);
//    slider1.setBounds (10, 110, getWidth() - 20, 40);
//    combobox1.setBounds (10, 160, getWidth() - 20, 40);
//    texteditor1.setBounds (10, 210, getWidth() - 20, 40);
//    colourSelector1.setBounds (10, 10, getWidth() - 20, getHeight() / 2.0);
//    mySnazzyButton.setBounds (10, 10, 40, 40);
    myButtonRow.setBounds(10, 10, getWidth() - 20, 40);
}

void MainComponent::paint( Graphics& g)
{
    g.setColour(colourSelector1.getCurrentColour());
    g.fillEllipse(x - 5, y - 5, 10, 10);
}

void MainComponent::mouseUp (const MouseEvent& event)
{
    DBG ("MOUSE UP!\nCoordinates: x - " << event.x << ", y - " << event.y << ".\n");
    x = event.x;
    y = event.y;
    repaint();
}

void MainComponent::buttonClicked (Button* button)
{
    if (button == &textButton1)
        DBG ("Button one likes you. Button two really doesn't like you.\n");
    else if (button == &textButton2)
        DBG ("Button two likes you. Button one hates you.\n");
}

void MainComponent::sliderValueChanged(Slider* slider)
{
    if (slider == &slider1)
        DBG ("Slider value changed. New value is " << slider1.getValue() << ".\n");
}

void MainComponent::comboBoxChanged (ComboBox* comboBoxThatHasChanged)
{
    if (comboBoxThatHasChanged == &combobox1)
        DBG ("'" << combobox1.getSelectedItemIndex() << "' has been chosen from the combo box.\n");
}

void MainComponent::textEditorTextChanged (TextEditor&)
{
    DBG ("There is new text in the text editor. Now it says '" << texteditor1.getText() << "'.\n");
}