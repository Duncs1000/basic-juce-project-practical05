/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "SnazzyButton.h"
#include "ButtonRow.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component, public Button::Listener, public Slider::Listener, public ComboBox::Listener, public TextEditor::Listener
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();

    void resized();
    void paint (Graphics& g) override;
    void mouseUp (const MouseEvent& event) override;
    
    void buttonClicked (Button* button) override;
    void sliderValueChanged (Slider* slider) override;
    void comboBoxChanged (ComboBox* comboBoxThatHasChanged) override;
    void textEditorTextChanged (TextEditor&) override;

private:
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
    TextButton textButton1, textButton2;
    Slider slider1;
    ComboBox combobox1;
    TextEditor texteditor1;
    ColourSelector colourSelector1;
    float x, y;
    SnazzyButton mySnazzyButton;
    ButtonRow myButtonRow;
};


#endif  // MAINCOMPONENT_H_INCLUDED
